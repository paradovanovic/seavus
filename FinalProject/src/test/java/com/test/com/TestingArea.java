package com.test.com;

import com.classes.com.Utilities.Browsers;
import com.classes.com.Utilities.SetupBrowsers;
import com.google.common.io.Files;
import com.sun.xml.internal.ws.commons.xmlutil.Converter;
import javafx.scene.shape.Path;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;

import java.io.*;
import java.io.File.*;
import java.util.Scanner;

public class TestingArea {

    WebDriver driver;
    WebDriverWait wait;
    SetupBrowsers stpbrowser;

    @Test
    public void FillEtsy() throws IOException, InterruptedException {

        stpbrowser = new SetupBrowsers(driver, wait); //redundant
        stpbrowser.setup(driver, wait, Browsers.GOOGLE);

        stpbrowser.testEtsy();

    }

//    (Failed attempt to bypass captcha
//    public void captcha()
//    {
//        stpbrowser = new SetupBrowsers(driver, wait); //redundant
//        stpbrowser.setup(driver, wait, Browsers.GOOGLE);
//
//        stpbrowser.navigate("http://www.deathbycaptcha.com/user/login");
//
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"recaptcha-anchor\"]/div[5]"))).click();
//    }
}
