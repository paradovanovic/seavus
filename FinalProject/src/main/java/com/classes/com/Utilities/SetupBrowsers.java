package com.classes.com.Utilities;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;

public class SetupBrowsers {
    WebDriver driver;
    WebDriverWait wait;

    public SetupBrowsers()
    {
    }

    public SetupBrowsers(WebDriver driver, WebDriverWait wait)
    {
        this.driver = driver;
        this.wait = wait;
    }

    public boolean isPresent(WebDriver driver, String xpath) //checks if an element is present and returns bool
    {
        try
        {
            driver.findElement(By.xpath(xpath));
            return true;
        }
        catch (NoSuchElementException e)
        {
            return false;
        }
    }

    public void navigate(String URL) //enter URL, go there, pretty simple
    {
        try
        {
            driver.get(URL);
        }
        catch (Exception e) {
            System.out.println("Please check the url");
        }
    }

    public void quit()
    {
        driver.quit();
    }

    public boolean check(String x, String y) //compares two strings, returns bool, obsolete
    {
        if (x == y) return true;
        else return false;
    }

    public void setup(WebDriver driver, WebDriverWait wait, Browsers browser)
    {
        switch(browser)
        {
            case GOOGLE:
                System.setProperty("webdriver.chrome.driver", "src/main/resources/Drivers/chromedriver.exe");
                driver = new ChromeDriver();
                wait = new WebDriverWait(driver, 10);
                break;
            case IE:
                System.setProperty("webdriver.ie.driver", "src/main/resources/Drivers/IEDriverServer.exe");
                driver = new InternetExplorerDriver();
                wait = new WebDriverWait(driver, 10);
                break;
            case MOZILLA:
                System.setProperty("webdriver.gecko.driver", "src/main/resources/Drivers/geckodriver.exe");
                driver = new FirefoxDriver();
                wait = new WebDriverWait(driver, 10);
                break;
        }
        this.driver = driver;
        this.wait = wait;

    }

    public void testEtsy() throws IOException, InterruptedException {
        try {

            FileReader fread = new FileReader("src/main/resources/toRead/names.txt");
            BufferedReader bread = new BufferedReader(fread);
            int state = 0;
            String line = "start";
            String[] tokens = new String[4];

            navigate("https://www.etsy.com");
            Assert.assertEquals("Etsy.com | Shop for anything from creative people everywhere", driver.getTitle());

            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"sign-in\"]"))).click();

            Thread.sleep(3000);

            if(isPresent(driver, "//*[@id=\"join_neu_email_field\"]")) //checks which sign-up form is open and takes necessary steps to avoid captcha
            {
                while (line != null)
                {
                    line = bread.readLine();
                    if (line != null)
                        tokens = line.split("\\s+");

                    driver.get("https://etsy.com");
                    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"register\"]"))).click();

                    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"join_neu_email_field\"]"))).sendKeys(tokens[0]);
                    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"join_neu_first_name_field\"]"))).sendKeys(tokens[1]);
                    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"join_neu_password_field\"]"))).sendKeys(tokens[0]+tokens[1]);
                    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"join-neu-form\"]/div[1]/div/div[5]/div/button"))).click();
                }
            }
            else {
                while (line != null) {

                    line = bread.readLine();  //Reads from "names.txt" and separates them into tokens
                    if (line != null)
                        tokens = line.split("\\s+");

                    if (state == 0) {
                        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"signin-button\"]"))).click();
                        state = 1;
                    }


                    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"username-existing\"]"))).clear();
                    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"password-existing\"]"))).clear();

                    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"username-existing\"]"))).sendKeys(tokens[0] + " " + tokens[1]);
                    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"password-existing\"]"))).sendKeys(tokens[1] + tokens[0]);

                    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"signin_button\"]"))).click();
                }
            }


        driver.quit();
        } catch (InterruptedException i) {
            i.printStackTrace();
            driver.quit();
        }
    }
}
